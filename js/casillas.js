class Casillas extends HTMLElement {

  constructor() {
    super();
	this.numeroCasillasIniciales = 10;
	this.numeroCaracteres = 4;
	this.tipoElemento = 'checkbox';
	this.nombre = 'gft';
  }
  
  connectedCallback() {
	this.respuesta = this.getRandomString();
    this.shadow = this.attachShadow({mode : 'open'});
	this.elementosDOM = this.crearCasillas(this.numeroCasillasIniciales);
  }
  
  disconnectedCallback() {
	alert('Fin del juego');
  }
  
  attributeChangedCallback(name, oldValue, newValue) {
    const PATRON = /^[0-9]*$/
	if (PATRON.test(newValue)) {
	  if (oldValue !== null) {
	    this.removerCasillas();
	    this.elementosDOM = this.crearCasillas(newValue);
	  }
	} else {
	  throw ('NumberFormatException');
	  this.elementosDOM = this.actualizar();
	}
  }
  
  static get observedAttributes() {
  	return ['casillas'];
  }
  
  actualizar() {
    this.removerCasillas();
	this.respuesta = this.getRandomString();
	return this.crearCasillas(this.numeroCasillasIniciales);
  }
  
  removerCasillas() {
    this.elementosDOM.forEach(element => {
	  this.shadow.removeChild(element);
	});
  }
  
  crearCasillas(cantidadElementos) {
    console.log(this.respuesta);
    let nodoLabel, nodoRadio;
	let nodoLabelText;
	let textoRandom;
	let elementosDOM = [];
	const posicionAleatorio = Math.floor(Math.random() * cantidadElementos);
	for (let i = 0; i < cantidadElementos; i++) {
	  textoRandom = this.getRandomString();		  
	  nodoRadio = this.setValorRadio(nodoRadio, (i + 1), textoRandom);		  		  
	  nodoLabelText = document.createTextNode(textoRandom);		  
	  nodoLabel = this.setValoresLabel(nodoLabel, nodoRadio, nodoLabelText);		  
	  elementosDOM.push(nodoLabel);
	}
	
	elementosDOM[posicionAleatorio] = this.setNodorespuestaCorrecta(posicionAleatorio);
	
	elementosDOM.forEach((element) => {
	  this.shadow.appendChild(element);
	});
	
	return elementosDOM;
  }
  
  setNodorespuestaCorrecta(id) {
    let nodoRadio, nodoLabel;
    nodoRadio = this.setValorRadio(nodoRadio, id, this.respuesta);
	let childLabelText = document.createTextNode(this.respuesta);
	nodoLabel = this.setValoresLabel(nodoLabel, nodoRadio, childLabelText);
	return nodoLabel;
  }
  
  setValoresLabel(nodoLabel, nodoRadio, nodoTextLabel) {
    nodoLabel = document.createElement('label');
    nodoLabel.appendChild(nodoRadio);
	nodoLabel.appendChild(nodoTextLabel);
	// nodoLabel.appendChild(document.createElement('br'));
	return nodoLabel;
  }
  
  setValorRadio(elemento, id, valor) {
    elemento = document.createElement('input');
    elemento.setAttribute('type', this.tipoElemento);
	elemento.setAttribute('id', id);
	elemento.setAttribute('name', this.nombre);
	elemento.setAttribute('value', valor);
	elemento.addEventListener('click', () => {
	  if (this.respuesta === valor) {
	    alert('¡Válido!');
		this.elementosDOM = this.actualizar();
	  } else {
	    alert('No es válido');
	  }
	});
	return elemento;
  }
  
  getRandomString() {
    const ABC = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
	let output = '';
	for (let i = 0; i < this.numeroCaracteres; i++) {
	  output = output + ABC[Math.floor(Math.random() * ABC.length)];
	}
	return output;
  }
  
}

customElements.define('ooas-loteria', Casillas);